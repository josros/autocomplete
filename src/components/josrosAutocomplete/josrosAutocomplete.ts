import { LitElement, html, customElement, TemplateResult, property, css, CSSResult, query } from 'lit-element';

export interface CompletionItem {
  text: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  value?: any;
}

const keyCodes = Object.freeze({
  enter: 13,
  tab: 9,
  delete: 46,
  space: 32,
  up: 38,
  down: 40,
  left: 37,
  right: 39,
  backspace: 8
});

@customElement('josros-autocomplete')
export class JosRosAutocomplete extends LitElement {
  private static readonly SUGGESTION_ID_PREFIX = 'suggestion-item-';

  private static readonly SELECTED_EVENT = 'selected';
  private static readonly INPUT_EVENT = 'autocomplete-input';
  private static readonly CHANGED_EVENT = 'changed';
  private static readonly BLURRED_EVENT = 'blurred';
  private static readonly FOCUSSED_EVENT = 'focussed';
  private static readonly ENTER_PRESSED_EVENT = 'enter-pressed';

  @property({ type: String })
  private value!: string;

  @property({ type: String })
  private label!: string;

  @property({ type: Array })
  private items!: CompletionItem[];

  @property({ type: Boolean })
  private loading = false;

  @property({ type: Boolean, attribute: 'no-border' })
  private noBorder = false;

  @property({ type: Boolean })
  private outline = false;

  @property({ type: Boolean, attribute: 'highlight-bottom-line' })
  private highlightBottomLine = false;

  @property({ type: Number, attribute: 'max-suggestion-items' })
  private maxSuggestionItems = 15;

  @property({ type: Boolean, attribute: false })
  private enabled = false;

  @property({ type: Number, attribute: false })
  private selectedIndex = -1;

  @property({ type: Array, attribute: false })
  private filteredItems!: CompletionItem[];

  @query('.height-restriction')
  private suggestionsScrollDiv!: HTMLDivElement;

  private filterBy(text: string): CompletionItem[] {
    if (!text) {
      return [];
    }

    return this.items
      .filter((item) => {
        return item.text.toLowerCase().includes(text.toLowerCase());
      })
      .sort((val1, val2) => {
        return (
          val1.text.toLowerCase().indexOf(text.toLowerCase()) - val2.text.toLowerCase().indexOf(text.toLowerCase())
        );
      })
      .slice(0, this.maxSuggestionItems);
  }

  private onInput(event: InputEvent): void {
    const currentText = (event?.target as HTMLInputElement).value;
    this.filteredItems = this.filterBy(currentText);
    // must be set here, otherwise value may not be selected twice:
    // https://github.com/Polymer/lit-element/issues/144#issuecomment-468098257
    this.value = currentText;

    const inputEvent = new CustomEvent(JosRosAutocomplete.INPUT_EVENT, {
      detail: currentText
    });
    this.dispatchEvent(inputEvent);
  }

  private onChange(event: InputEvent): void {
    const currentText = (event?.target as HTMLInputElement).value;

    const changedEvent = new CustomEvent(JosRosAutocomplete.CHANGED_EVENT, {
      detail: currentText
    });
    this.dispatchEvent(changedEvent);
  }

  private onBlur(event: InputEvent): void {
    this.filteredItems = [];
    this.enabled = false;
    const currentText = (event?.target as HTMLInputElement).value;

    const blurredEvent = new CustomEvent(JosRosAutocomplete.BLURRED_EVENT, {
      detail: currentText
    });
    this.dispatchEvent(blurredEvent);
  }

  private onFocus(event: InputEvent): void {
    const currentText = (event?.target as HTMLInputElement).value;
    this.filteredItems = this.filterBy(currentText);
    this.enabled = true;

    const focussedEvent = new CustomEvent(JosRosAutocomplete.FOCUSSED_EVENT);
    this.dispatchEvent(focussedEvent);
  }

  private onUpDown(event: KeyboardEvent): void {
    event.preventDefault();
    const keyCode = event.keyCode;

    if (keyCode === keyCodes.down) {
      this.keyDown();
    } else if (keyCode === keyCodes.up) {
      this.keyUp();
    }
    this.updateScoll();
  }

  private updateScoll(): void {
    const selectedItem = this.shadowRoot?.querySelector(
      `#${JosRosAutocomplete.SUGGESTION_ID_PREFIX}${this.selectedIndex}`
    ) as HTMLDivElement;
    if (selectedItem) {
      this.suggestionsScrollDiv.scrollTop = selectedItem.offsetTop;
    }
  }

  private keyDown(): void {
    if (this.selectedIndex === this.filteredItems.length - 1) {
      this.selectedIndex = 0;
    } else {
      this.selectedIndex = this.selectedIndex + 1;
    }
  }

  private keyUp(): void {
    if (this.selectedIndex === 0) {
      this.selectedIndex = this.filteredItems.length - 1;
    } else {
      this.selectedIndex = this.selectedIndex - 1;
    }
  }

  private selectItem(item: CompletionItem): void {
    this.value = item.text;
    this.reset();

    const enteredEvent = new CustomEvent(JosRosAutocomplete.SELECTED_EVENT, {
      detail: item
    });
    this.dispatchEvent(enteredEvent);
  }

  private reset(): void {
    this.selectedIndex = -1;
    this.filteredItems = [];
  }

  private onEnter(): void {
    if (this.filteredItems.length > 0 && this.selectedIndex <= this.filteredItems.length) {
      const currentSelectedItem = this.filteredItems[this.selectedIndex];
      this.selectItem(currentSelectedItem);
    } else {
      const enterPressedEvent = new CustomEvent(JosRosAutocomplete.ENTER_PRESSED_EVENT);
      this.dispatchEvent(enterPressedEvent);
    }
  }

  private onKeyDown(event: KeyboardEvent): void {
    const keyCode = event.keyCode;

    if (keyCode === keyCodes.up || keyCode === keyCodes.down) {
      this.onUpDown(event);
    } else if (keyCode === keyCodes.enter) {
      this.onEnter();
    }
  }

  private onSuggestionMouseDown(event: MouseEvent): void {
    event.preventDefault();
    const clickedDivId = (event?.target as HTMLDivElement).id;
    const clickedIndex = parseInt(clickedDivId.replace(JosRosAutocomplete.SUGGESTION_ID_PREFIX, ''));
    const clickedItem = this.filteredItems[clickedIndex];
    this.selectItem(clickedItem);
  }

  render(): TemplateResult {
    return html`
      <div class="full-width">
        ${this.renderInput()}
        <div class="suggestions">
          <div class="border height-restriction">
            ${this.filteredItems && this.filteredItems.length > 0 ? this.renderItems() : ''}
          </div>
        </div>
      </div>
    `;
  }

  renderInput(): TemplateResult {
    return html`
      <div class="input-container full-width ${this.enabled ? 'enabled' : ''} ${this.loading ? 'loading' : ''}">
        <input
          id="josros-autocomplete--input"
          class="${this.noBorder ? 'input-no-border' : 'input-border-default'} ${this.outline
            ? ''
            : 'input-no-outline'}"
          type="text"
          .value="${this.value ? this.value : ''}"
          placeholder="${this.enabled ? '' : this.label}"
          @blur="${this.onBlur}"
          @input="${this.onInput}"
          @change="${this.onChange}"
          @focus="${this.onFocus}"
          @keydown="${this.onKeyDown}"
        />
        ${this.noBorder || !this.highlightBottomLine
          ? ''
          : html`
              <span class="input--focus-line"></span>
            `}
        <span class="input--loading-line"></span>
        <label class="input--label" htmlFor="josros-autocomplete--input">${this.label}</label>
      </div>
    `;
  }

  // mousedown instead of click, because input blur event would prevent the click event
  renderItems(): TemplateResult {
    return html`
      <div>
        ${this.filteredItems.map(
          (item, index) =>
            html`
              <div
                id="${JosRosAutocomplete.SUGGESTION_ID_PREFIX}${index}"
                @mousedown="${this.onSuggestionMouseDown}"
                class="suggestion-item ${this.selectedIndex === index ? 'suggestion-item--selected' : ''}"
              >
                ${item.text}
              </div>
            `
        )}
      </div>
    `;
  }

  static get styles(): CSSResult {
    return css`
      :host {
        font-size: var(--josros-autocomplete--font-size, 1rem);
        --primary-color: #000;
        --background-color: transparent;
        --foreground-color: rgba(0, 0, 0);
        --item-background-color: lightgray;
        --item-focus-color: #e0e0e0;
        --loader-color: rgb(0, 136, 204);
        --font-size: 1.6em;
        --padding: 1.2em 0em 0.4em 0em;
        --padding--label: 0em;
        --padding--suggestion: 1.5em;
        --margin--suggestions: 0em;
        --max-height--suggestions: 20em;
        --input-default-border: none;
        --input-default-border-bottom: 0.1em solid var(--primary-color, grey);
      }

      .full-width {
        width: 100%;
      }

      .input-container {
        position: relative;
      }

      input[type='text'] {
        width: 100%;
        padding: var(--padding, 1.6em);

        font-size: var(--font-size, 2.4em);

        background-color: var(--background-color, transparent);
        color: var(--foreground-color, #000);
      }

      .input-border-default {
        box-sizing: border-box;
        border: var(--input-default-border, none);
        border-bottom: var(--input-default-border-bottom, 1px solid var(--primary-color, grey));
      }

      .input-no-border {
        box-sizing: border-box;
        border: none;
      }

      .input-no-outline {
        outline: none;
      }

      .enabled .input--focus-line {
        opacity: 100%;
      }
      .input--focus-line {
        display: block;
        position: relative;
        width: 100%;
        height: 1px;
        background: var(--primary-color, #000);
        opacity: 0;
        transition-property: opacity, background;
        transition-duration: 0.25s;
      }

      .loading .input--loading-line {
        opacity: 100%;
      }
      .input--loading-line {
        display: block;
        position: absolute;
        bottom: 0;
        height: 0.3em;
        background: var(--loader-color, rgb(0, 136, 204));
        opacity: 0;
        animation-name: infinite-loading;
        animation-duration: 1.5s;
        animation-iteration-count: infinite;
      }

      @keyframes infinite-loading {
        0% {
          left: 0%;
          right: 90%;
        }

        60% {
          left: 30%;
          right: 30%;
        }

        100% {
          left: 100%;
          right: 0%;
        }
      }

      .input--label {
        display: block;
        position: absolute;
        padding: var(--padding--label, 0em);
        top: 0;
        right: 0;
        left: 0;
        width: 100%;
        color: var(--foreground-color, #000);
        font-size: inherit;
        font-weight: 400;
        text-align: left;
        white-space: nowrap;
        overflow: hidden;
        text-overflow: ellipsis;
        transition-property: top, font-size, color, opacity;
        transition-duration: 0.25s;
        transition-timing-function: cubic-bezier(0.75, 0.02, 0.5, 1);
        opacity: 0 !important;
        pointer-events: none;
      }
      .enabled .input--label {
        font-size: 1.2em;
        opacity: 1 !important;
      }
      .suggestions {
        position: relative;
        background-color: var(--item-background-color, lightgrey);
        z-index: 99;
        margin: var(--margin--suggestions, 0px);
      }

      .suggestion-item {
        padding: var(--padding--suggestion, 1.5em);
      }

      .suggestion-item:hover {
        background-color: var(--item-focus-color, #e0e0e0);
      }

      .suggestion-item--selected {
        background-color: var(--item-focus-color, #e0e0e0);
      }

      .suggestions .border {
        -webkit-box-shadow: 0em 0.3em 1.3em 0em rgba(0, 0, 0, 0.3);
        -moz-box-shadow: 0em 0.3em 1.3em 0em rgba(0, 0, 0, 0.3);
        box-shadow: 0em 0.3em 1.3em 0em rgba(0, 0, 0, 0.3);
      }

      .height-restriction {
        max-height: var(--max-height--suggestions, 20em);
        overflow: auto;
      }
    `;
  }
}
