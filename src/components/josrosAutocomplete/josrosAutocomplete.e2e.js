/* eslint-disable @typescript-eslint/explicit-function-return-type */
import { PuppeteerHelper } from '../../testutil/puppeteerHelper';

/*
 NOTE that you must serve the demo page on port 3000 to make this test work
*/
describe('josrosAutocomplete component', () => {
  let browser;
  let page;
  let helper;

  beforeEach(async () => {
    browser = await PuppeteerHelper.launchBrowser();
    page = await browser.newPage();
    helper = new PuppeteerHelper(page, 'josros-autocomplete');
    await page.goto('http://localhost:3000/src/components/josrosAutocomplete/josrosAutocomplete.html');
  });

  afterEach(async () => {
    await browser.close();
  });

  it('Label and focus works', async () => {
    const testLabelText = 'My Testlabel';
    await helper.setAttribute('label', testLabelText);
    await helper.setAttribute('highlight-bottom-line', 'true');

    await expect(await helper.attributeOf("input[type='text']", 'placeholder')).toEqual(testLabelText);
    await expect(await helper.textContentOf('.input--label')).toEqual(testLabelText);
    await expect(await helper.cssPropertyOf('.input--label', 'opacity')).toEqual('0');
    await expect(await helper.cssPropertyOf('.input--focus-line', 'opacity')).toEqual('0');

    await helper.focus('input[type=text]');
    await page.waitFor(1000); // wait for the animation to be completed

    await expect(await helper.attributeOf("input[type='text']", 'placeholder')).toEqual('');
    await expect(await helper.cssPropertyOf('.input--label', 'opacity')).toEqual('1');
    await expect(await helper.cssPropertyOf('.input--focus-line', 'opacity')).toEqual('1');
  });

  // FIXME listenFor does not work
  // it('Fires input event', async () => {
  //   const onInput = await helper.listenFor('autocomplete-input');

  //   await helper.type('input[type=text]', 'T');
  //   await page.waitFor(100);

  //   expect(onInput).toHaveBeenCalledWith(expect.objectContaining({
  //     detail: 'T'
  //   }));
  // });

  it('Shows suggestions', async () => {
    await helper.setAttribute(
      'items',
      JSON.stringify([{ text: 'Hello' }, { text: 'Hallo' }, { text: 'Hola' }, { text: 'Bonjour' }])
    );

    await helper.type('input[type=text]', 'H');
    await page.waitFor(100);

    await expect(await helper.textContentOf('#suggestion-item-0')).toEqual(expect.stringContaining('Hello'));
    await expect(await helper.textContentOf('#suggestion-item-1')).toEqual(expect.stringContaining('Hallo'));
    await expect(await helper.textContentOf('#suggestion-item-2')).toEqual(expect.stringContaining('Hola'));
  });
});
