# josros-autocomplete

A basic auto-completion Web Component.

[Demo](http://josros.gitlab.io/autocomplete)

## Installation

1. Install dependency

    `npm install @josros/autocomplete --save`

1. ES6 import

    `import "@josros/autocomplete";`

1. Use the component

    ```html
    <josros-autocomplete
        label='Your label'
    ></josros-autocomplete>
    ```

## Usage

### Set data

Here is how you can set the `items` property on a normal website:

```js
document.querySelectorAll('josros-autocomplete').forEach(element => {
    element.setAttribute('items', JSON.stringify([{'text': 'Hello'}, {'text': 'Hallo'}, {'text': 'Hola'}, {'text': 'Bonjour'}]));
})
```

### Component Api

#### Properties

| Attribute       | Description                                                                          | Type      | Default |
| --------------- | ------------------------------------------------------------------------------------ | --------- | ------- |
| `value` | Initial value of the text field | `String` | `""`  |
| `label`     | Label of the text field | `String` | `""` |
| `items`      | Auto completion items (suggestions) | `[{ text!: string, value?: any}]` | `[]` |
| `loading`         | Toggles loading state | `boolean` | `false` |
| `max-suggestion-items`         | Max. elements suggested | `number`  | `15`    |
| `no-border`         | Disables borders around input text field | `boolean`  | `false`    |
| `outline`         | Disables outline around input text field | `boolean`  | `false`    |
| `highlight-bottom-line`         | Enables bottom line highlighting around input text field on focus (disabled if noBorder is defined) | `boolean`  | `false`    |

#### Events

| Event        | Description                                                           |
| ------------ | --------------------------------------------------------------------- |
| `selected` | Fired as soon as a suggestion is selected (via keyboard or mouse) |
| `autocomplete-input` | Fires on text changes |
| `changed` | Fires on internal input change event |
| `blurred` | Fires on internal input blur event |
| `focussed` | Fires on internal input focus event |
| `enter-pressed` | Fire as soon as enter is pressed on the input |

#### CSS Custom Properties

| Name                          | Description                      | Default                      |
| ----------------------------- | -------------------------------- | -------------------------------- |
| `--josros-autocomplete--font-size`          | The font-size of the host                       | 1rem
| `--primary-color`          | Primary color                       | #000
| `--background-color`       | Background color                    | transparent
| `--foreground-color`       | Foreground color                    | rgba(0, 0, 0)
| `--item-background-color` | Background color of the suggestion items | `lightgrey`
| `--item-focus-color`       | Background color of the suggestion items shown when input gets focus   | #e0e0e0
| `--loader-color`           | Color of the loading animation      | rgb(0, 136, 204)
| `--font-size`              | Font size of the input text         | 1.6em
| `--padding`                | Padding of the input                | 1.2em 0em 0.4em 0em
| `--padding--label`         | Padding of the label above the input  | 0em
| `--padding--suggestion`    | Padding of a single suggestion box  | 1.5em
| `--margin--suggestions`    | Margin of the whole suggestion container  | 0em
| `--max-height--suggestions`| Max height of the suggestion container  | 20em
| `--input-default-border`   | Border around the input text field  | none
| `--input-default-border-bottom`    | Bottom border around the input text field  | 0.1em solid var(--primary-color, grey)

## Execution

```bash
# production build
npm run build

# run dev server at http://localhost:3000
npm run serve

# unit tests
npm run test:unit

# e2e tests
npm run test:e2e

# Lintting
npm run lint
```

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## License

BSD 3-Clause License