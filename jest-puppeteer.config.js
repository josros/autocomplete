module.exports = {
  launch: {
    args: ['--no-sandbox', '--disable-setuid-sandbox', '--disable-web-security']
  },
  server: {
    command: 'npm run serve',
    port: 3000,
    launchTimeout: 10000
  }
};
